This work was published as part of a journal paper:
J Bellizzi, M Vella, C Colombo, Hernandez Castro Julio - ICISSP 2022 
[Using Infrastructure-Based Agents to Enhance Forensic Logging of Third-Party Applications](http://staff.um.edu.mt/__data/assets/pdf_file/0019/511075/ICISSP_2023_31_CR.pdf)


# Infrastructure-based Forensic Logging Agent

`sqlite_driver.js` is a forensic logging agent intended to work with [JIT-MF](https://gitlab.com/mobfor/jitmf_experiments_resources) as a JIT-MF driver. This agent logs SQLite queries executed during the runtime of an Android application.

The `post_processor.py` includes logic to parse WhatsApp, Telegram and Signal log entries (query statements) produced by the `sqlite-driver.js`.

